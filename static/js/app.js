var mapboxTiles = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
});


var langs = { "am":"Amharic", "ar":"Arabic", "bg":"Bulgarian", "bn":"Bengali", "bo":"Tibetan", "chr":"Cherokee", "da":"Danish", "de":"German", "dv":"Maldivian", "el":"Greek", "en":"English", "es":"Spanish", "fa":"Persian", "fi":"Finnish", "fr":"French", "gu":"Gujarati", "iw":"Hebrew", "hi":"Hindi", "hu":"Hungarian", "hy":"Armenian", "in":"Indonesian", "is":"Icelandic", "it":"Italian", "iu":"Inuktitut", "ja":"Japanese", "ka":"Georgian", "km":"Khmer", "kn":"Kannada", "ko":"Korean", "lo":"Lao", "lt":"Lithuanian", "ml":"Malayalam", "my":"Myanmar", "ne":"Nepali", "nl":"Dutch", "no":"Norwegian", "or":"Oriya", "pa":"Panjabi", "pl":"Polish", "pt":"Portuguese", "ru":"Russian", "si":"Sinhala", "sv":"Swedish", "ta":"Tamil", "te":"Telugu", "th":"Thai", "tl":"Tagalog", "tr":"Turkish", "ur":"Urdu", "vi":"Vietnamese", "zh":"Chinese" };




//heatmap


var map = L.map('map')
    .addLayer(mapboxTiles)
    .setView([33.7677129, -84.420604], 10);

var heatmap = new L.TileLayer.HeatCanvas({}, {
    'step': 0.5,
    'degree': HeatCanvas.LINEAR,
    'opacity': 0.5
});

var markerLayers = new L.LayerGroup();
map.addLayer(heatmap);
map.addLayer(markerLayers);

// $(function() {
//     $( "#datepicker" ).datepicker();
//   });
//
//   $(function() {
//     $( "#slider-range" ).slider({
//       range: true,
//       min: 0,
//       max: 24,
//       values: [ 0, 24],
//       slide: function( event, ui ) {
//         $( "#amount" ).val( "from " + ui.values[ 0 ] + " till " + ui.values[ 1 ] );
// 		window.startingHour = ui.values[ 0 ];
// 		window.endingHour = ui.values[ 1 ];
// 		//console.log(window.startingHour);
// 		//console.log(window.endingHour);
//       }
//     });
//   });

  //$.get('api/languages').done(function(langs){
  //var langs = ['English','Russian','Arabic'];
  $.each(langs, function(index, value){
	$('#langSelector').append('<option value='+index+'>'+value+'</options>');
  });
  //});

  //$('#langSelector').on('click', function(){
	//window.languageSelected = $('#langSelector').val();
	//console.log(window.languageSelected);
  //});



  $('#go').on('click', function(){
	$.ajax({
		url: 'api/data',
	}).done(function(data){
    $.each(data.result, function(index, langitem){
      if(langitem._id === $('#langSelector').val()){

            console.log(langitem.coords);
        for (var i = 0, l = langitem.coords.length; i < l; i++) {
          var coord = langitem.coords[i];

          heatmap.pushData(coord[1], coord[0], 30);
          /*if (data[i][2] > 20) {
              var marker = new L.Marker(new L.LatLng(data[i][0], data[i][1]));
              marker.bindPopup(data[i].toString());
              markerLayers.addLayer(marker);
          }*/
          heatmap.redraw();
        }
      }
    });
	  });
    return false;
  });
